## Install WSL manually (using Windows)

```
https://www.it-connect.fr/installer-wsl-2-sur-windows-10/
```

## Git
```
cd existing_repo
git remote add origin https://gitlab.com/azure-sdk-python/access_reviews.git
git branch -M main
git push -uf origin main
```

## Install Python and Pip

```
sudo apt install python3
sudo apt install pip
sudo pip install virtualenv
```

## Create virtualenv and activate it

```
virtualenv venv
source venv/bin/activate
```
## Install dependencies

```
pip install -r requirements.txt
```

## Add those env variables

```
export subscription_id=""
export tenant_id=""
export app_id=""
export secret=""

```
## Run the script 

```
python3 main.py
```

