from azure.mgmt.authorization import AuthorizationManagementClient
from azure.identity import ClientSecretCredential
import os
import json

subscription_id = os.environ["subscription_id"]
tenant_id = os.environ["tenant_id"]
application_id = os.environ["app_id"]
application_secret = os.environ["secret"]

credSP = ClientSecretCredential(tenant_id=tenant_id, client_id=application_id, client_secret=application_secret)

client = AuthorizationManagementClient(credential=credSP, subscription_id=subscription_id)

role_assignements = client.role_assignments.list_for_subscription()

for role_assignment in role_assignements:
    role_definition = client.role_definitions.get_by_id(role_assignment.role_definition_id)

    if role_definition.role_name == 'Owner':
        print(f"User {role_assignment.principal_id} is an 'Owner'")

#for role in roles_sub:
   #roles_sub_json = role.as_dict()
   #print(json.dumps(roles_sub_json, indent=4))
